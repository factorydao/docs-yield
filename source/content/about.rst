About yield.vote
==================
| If you felt confused on yield.vote site then you are in the right place.
| We'll try to simply explain how to play and to not get confused again.

.. |A| image:: ../images/hoverPR.png
    :height: 50px

.. |B| image:: ../images/hover.png
    :height: 50px

.. container:: box
    
    .. container:: left
        
        This is what you see right after entering yield.vote:   
        
    .. container:: right
        
        |A|


.. container:: templ

    .. container:: templhover

        .. figure:: ../images/mainpage_1.png
            :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
            :width: 90%

    .. container:: templbasic

        .. figure:: ../images/mainpage_1.jpg
            :width: 90%

    
| We truly hope that this part is not confusing, but to be absolute clear, let us explain what are you looking at.
| 
| The most important thing here is to understand that this is kind of a template which will be created for every partner who would like to create its own pools.
| In the finance.vote pools section there are 3 boxes for 3 different types of pools. We called them basic, resident and tycoon(which is actually a type of resident pool).
|
| As you can see, our partner's pools are named completely different (ITG Blue Lagoon Pool, ITG Reef Pool and ITG Krill Pool) but their working is based on the same mechanics as finance.vote pools. 

|B|

.. container:: templ

    .. container:: templhover

        .. figure:: ../images/1_mainppage_templ2.jpg
            :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
            :width: 90%

    .. container:: templbasic

        .. figure:: ../images/1_mainppage_templ.jpg
            :width: 90%

Basic pools
------------
| When you first enter basic pool you probably think that there is too much information. But keep calm, we will go through this together. 
| Actually it is not that complicated, since most of the elements are just informational, but yes, that could be overwhelming at first. 
| 
| Basic pool is an ultra simple financial primitive for those who don’t want to be LPs, but want to get FVT token yield.
| The contract opens at a specified time(**start date**) and reaches maturity at a specified time(**maturity date**). You see a quote for the yield(**APY**) you will receive on maturity and as long as you send your tokens before the pool cap is full, you get your quoted yield guaranteed from the timestamp of the block in which your transaction is mined.
|

.. container:: box

    .. container:: left

        Let's take a look on Basic Pool site: 
    
    .. container:: right
    
        |A|

.. container:: templ

    .. container:: templhover, templhover-basic

        .. figure:: ../images/2_basic1.png
            :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
            :width: 90%

    .. container:: templbasic

        .. figure:: ../images/2_basic1.jpg
            :width: 90%

| Hovering on the image releases another graphics which probably is a little disturbing for now, but don’t worry, once you’ll understand what's what, the schema becomes more helpful.
| Let's start decrypting every element of the site:

1. On the left side there is a menu panel with shortcuts to all available pools. 

2. Right below the name of the pool, there are 3 parameters:

    * **Total Value Locked** is a total amount of FVT currently deposited in the pool (from all user total). 
    * **APY** stands for “Annual Percentage Yield”. It is the amount of yield you would get on your principal (what you put in) annualised. In Basic Pool, your quotes are literal. What you see is what you get.
    * **Filled** stands for percentage of current pool filling (from all user total).
3. Pool details field

   * **Maximum Pool Size** - information on the pool size, meaning how much FVT can be deposit in the pool 
   * **Volume** - information on how much FVT where deposited into the pool in the last 24 hours (from all user total)
   * **Current Yield** - information on current percentage (how much user will get at the maturation). **(!) This parameter decreases with pool maturing.**
   * **Start Date** - time when contract opens
   * **Maturity Date** - time when contract reaches maturity and gets closed

4. Deposit field is something like a bank window, where you cen deposit your FVT and send them to the pool. ``MAX`` button allows deposit a maximum allowed amount of FVT, considering your FVT balance and pool capacity. 
5. List of open and closed transactions shows only yours open and closed positions (not other users):
   
   1. Open Positions elements:

      * **Transaction** - hash of deposit transaction 
      * **Deposit** - amount of FVT deposited
      * **Current Yield** - amount of yield that has grown till this moment (what you would get right now if you would make a withdrawal)
      * **Additional Rewards** - amount of any additional rewards
      * **Yield at Maturity** - amount of the yield you will get when the pool reaches maturity
      * **Withdraw** button used for funds withdrawals
    
    .. image:: ../images/2_basic_example4.jpg
        :align: center

   2. Closed Positions elements:

      * **Deposit Transaction** - hash of deposit transaction
      * **Deposit** - amount of FVT deposited
      * **Yield** - amount of yield collected
      * **Withdraw Transaction** - hash of withdraw transaction

    .. image:: ../images/2_basic_example5.jpg
        :align: center
    
    

Deposit into Basic Pool
^^^^^^^^^^^^^^^^^^^^^^^^
1. Enter deposit amount or use MAX button.
2. Hit Deposit button.
3. Confirm transaction fees in MetaMask pop up window.
4. If no error occurs, ``A deposit has been made!`` notification will pop up. 
5. Finished transaction will be shown on list of open transactions. 

| Let's explain this more detailed over an example:

.. admonition:: Example

    Carla enters the Basic Pool and she sees that maximum pool size is 10,000,000 FVT but currently only 43,672 FVT are deposited into the pool (and that's 0.437% filled). 

    She sees that if she deposit now her yield at pool maturation will be 12.93%, so she decides to deposit 100 FVT (which will give her additional 12.93 FVT at pool maturity).
   
    .. image:: ../images/2_basic_example2.jpg
        :width: 70%
    
    She enters 100 FVT into Deposit Amount field and hits Deposit button. 
    MetaMask notification pops up and she has to confirm transaction fees.

    She waits a while till deposit is send to the pool and then she can see her transaction on the list of open transactions. 

    .. image:: ../images/2_basic_example3.jpg
        :width: 70%
    
    She also noticed that Volume field and Total Value Locked (TVL) has changed and now are enlarged by her deposit. 

Withdraw from Basic Pool
^^^^^^^^^^^^^^^^^^^^^^^^^
1. Hit Withdraw button on the open positions list.
2. Information on deposit funds, current yield, yield at pool maturity and additional rewards will be shown as a pop up window. Read them carefully and keep in mind that once you'll withdraw your funds you won't be able to enter the pool on the same conditions again. 
3. Click Withdraw.
4. Confirm transaction fees in MetaMask pop up window.
5. If no error occurs, ``Successful payout`` notification will pop up.
6. Withdrawn position will be shown on list of closed positions.

| Let's explain this more detailed over an example:

.. |withdraw1| image:: ../images/2_basic_withdraw3.jpg
    :width: 49%

.. |withdraw2| image:: ../images/2_basic_withdraw4.jpg
    :width: 49%


.. admonition:: Example

    Carl entered the Basic Pool much sooner than Carla, that's why his yield at maturity percent was higher (14.23%). Now he wants to withdraw his funds from the pool. 
    
    .. image:: ../images/2_basic_withdraw1.jpg
        :width: 70%

    He hits Withdraw button on the open positions list, which opens a pop up window with information on deposit funds, current yield, yield at pool maturity and additional rewards. He should take a minute and think whether he really needs to take out his fund because once he'll done it, there will be no coming back. And what's more, any further deposits will be less profitable since yield percent decreases as pool matures. 

    .. image:: ../images/2_basic_withdraw2.jpg
        :width: 70%


    Carl however is decided and clicks Withdraw button and confirms transaction fees in popped up MetMask window. 
    
    He waits a while and then he sees that his transaction disappeared from open transactions list but appeared on the closed positions list instead. 

    |withdraw1| |withdraw2|

    He also noticed that Total Value Locked (TVL) has changed and now is reduced by his withdrawal. 


Resident Pools
--------------
| Once you've learned how Basic Pool works, it shouldn't be too hard to understand how Resident Pool works. 
|
| Resident Pool is a new kind of liquidity mining.
| As a user you will gain access to network emission by holding a protocol property.
| Protocol property is a slot in a smart contract that provides you access to the liquidity incentive token inventory of the finance.vote protocol.
| Resident Pool uses ‘Harberger Taxes’ to broker ownership of these slots in an on-going manner.

.. container:: box

    .. container:: left

        Main Resident Pool page looks alike to Basic Pool page. 
    
    .. container:: right
    
        |A|

.. container:: templ

    .. container:: templhover

        .. figure:: ../images/3_residenthover.png
            :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
            :width: 90%

    .. container:: templbasic

        .. figure:: ../images/3_residenthover.jpg
            :width: 90%

| Hovering on the image above releases another graphics but you shouldn't be surprised with this anymore.
| Again, let's decrypt every element of this site, as we did before:


1. On the left side there is a menu panel with shortcuts to all available pools. 

2. Right below the name of the pool, there are 2 parameters:

    * **Total Liquidity** is a total amount of LP Tokens staked in the pool (from all user total). 
    * **Average APE PY** stands for “Average Pulse Expected Percentage Yield”. 
    * **Average APY** stands for “Average Annual Percentage Yield”. 
    
3. Pool details field

   * **Volume** - how much LP Tokens where staked in the last 24 hours (from all user total)
   * **Minimum Deposit** - minimum amount of LP Tokens that can be staked 
   * **Maximum Deposit** - maximum amount of LP Tokens that can be staked 
   * **Lots filled** - number of currently occupying lots
   * **Current Pulse Length** - how many blocks will be in the current pulse
   * **Slot Rewards / Pulse** - rewards distributed per pulse

4. Below Pool details there is a field with two information:

    * **Lots** - number of yours occupied lots.
    * **Total rewards** - a sum of rewards from all your currently occupying lots. If you'll pay out any rewards *total rewards* won't change, but will if you'll vacate a Lot or get kicked out from it (*Total rewards* decrease by the number of total rewards from that Lot). 

5. On the right side there is a chart showing rewards in the current pulse. Since it’s not a flat rate, it starts off higher and then lowers quadratically. So the earlier you get in, the more you’ll earn.
6. Last element is the list of Occupied Lots. Here you can see every occupied Lot, regardless of whether it is occupied by you or not. To see only yours lots (if you have any) choose My Lots tab. Elements of list of Occupied lots:

    * **Lot No** - occupied property number
    * **Occupant** - wallet address of occupant
    * **Deposit** - current Lot price (initial deposit constantly decreasing by burn rate per pulse). This is also minimal amount of LP Tokens required to claim this Lot
    * **Burn rate** - amount of LP tokens that are burned per pulse
    * **Rewards** - the amount of profit from the property occupying. It is calculated from the start of Lot occupying and if you withdraw some funds during occupying the total amount won't change.
    * **APEPY**  - Average Pulse Expected Percentage Yield for the specific Lot
    * **Claim Lot** - button used for taking over the Lot
    * **Withdraw** (button shown only on your Lots) - button used for withdraw current available rewards

    .. image:: ../images/3_residentlot.jpg
        :align: center

Staking LP Tokens (Occupying Lot)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| Before we dive into this, you can see how Resident Pool works in Dr Nick's short overview video:

.. raw:: html
    
    <p align="center"><iframe width="560"
    height="315"
    src="https://www.youtube.com/embed/wxWCjCcyw1c"
    title="YouTube video player"
    style="border:4px solid #96E048;"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
    
| To become a property owner you'll have to stake LP tokens. We assume you've already have them (if not, see here `how to gain some <https://financevote.readthedocs.io/projects/yield/en/latest/content/token.html>`_).

1. Hit ``Stake LP Tokens`` button.
2. From the left column choose number of Lot you would like to occupy (every number has its own personalized image).

    .. container:: templ
    
        .. container:: templhover
    
            .. figure:: ../images/4_staking4.png
                :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
                :width: 90%
    
        .. container:: templbasic
    
            .. figure:: ../images/4_staking4.jpg
                :width: 90%
    
   You can occupy either vacant or already occupied Lot. The difference between them is initial **minimum LP tokens deposit** and initial **Burn Rate**. With the vacant Lots, those indexes are set by pool. With the occupied ones they are set by occupant and you have to outbid both of that to take over the Lot.

   .. container:: templ
    
        .. container:: templhover

            .. figure:: ../images/4_staking1.png
                :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
                :width: 90%

        .. container:: templbasic

            .. figure:: ../images/4_staking1.jpg
                :width: 90%

3. Set Deposit Amount (or New Deposit in occupied Lot).
4. Set Burn Rate (or New Burn Rate in occupied Lot). This index is the amount of LP tokens that are burned per pulse and it is strictly connected with the ‘Harberger Tax’. 

    .. container:: templ
    
        .. container:: templhover
    
            .. figure:: ../images/4_staking2.png
                :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
                :width: 90%
    
        .. container:: templbasic
    
            .. figure:: ../images/4_staking2.jpg
                :width: 90%

5. Current APEPY (or New APEPY) will calculate automatically. If you are not satisfied with its rate, you can try to set different Burn Rate. *hint: the smaller Burn Rate, the better APEPY is*.
6. Hit ``Claim Lot`` button. 
7. Confirm transaction fees in MetaMask pop up window.
8. If no error occurs, ``A deposit has been made!`` notification will pop up. 
9. Claimed Lot will be shown on the list in My Lots tab.

    .. image:: ../images/4_staking3.jpg
        :width: 90%

Withdraw or Vacate 
^^^^^^^^^^^^^^^^^^^^^
1. Press withdraw button

    .. image:: ../images/5_withdrawal.jpg
        :width: 90%

2. Window with details will pop up. Here you can check what was your **initial deposit**, how much LP was **burned** till now and how many **blocks till the end of current pulse** left. 
You can either **withdraw** available rewards or **vacate the lot**. When you vacate the lot, you gain available rewards and all **unburned LP that has left** (if there is any). Withdrawal lets you gain rewards while keeping the lot.

    .. container:: templ
    
        .. container:: templhover
    
            .. figure:: ../images/5_withdrawal2.png
                :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html
                :width: 90%
    
        .. container:: templbasic
    
            .. figure:: ../images/5_withdrawal2.jpg
                :width: 90%

3. Press ``Withdraw Rewards`` or ``Vacate Lot`` button.
4. Confirm transaction fees in MetaMask pop up window.
5. If no error occurs, ``Successful payout!`` or ``Successful vacate!`` notification will pop up. Either option, **Total Rewards** field will enlarge by the amount of gained rewards. If you chose to Vacate option, besides **Total Rewards** enlarge, **the Lots counter** will also decrease by 1.

        .. image:: ../images/5_totalrewards.jpg
            :width: 97%
            :align: center

    

        .. container:: templ

             .. container:: templhover

                 .. figure:: ../images/5_withdrawaltotal2.png
                     :target: https://financevote.readthedocs.io/projects/yield/en/latest/content/about.html

             .. container:: templbasic

                 .. figure:: ../images/5_withdrawaltotal.jpg

 
Tycoon Pools
------------
| Not available yet :) 