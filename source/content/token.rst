Tokens gaining
===============
.. important:: 
    Below descriptions concern receiving test tokens and providing liquidity on ropsten testnet **BUT** it works the same on the mainnet and those instructions can be used to gain FVT and LP tokens in mainnet as well. 

| To test our app, first of all, you'll have to have some ether on **ropsten testnet**. To gain some, you can use one of the ropsten ethereum faucet. Unfortunately, faucets dries out fast, so it is a challenge to find a running one. 
| Happily, this one is running for now: `Faucet here <https://faucet.dimensions.network/>`_ ( we assume, you've already have MetaMask added to your browser and do have a wallet address. If not, just `go here <https://metamask.zendesk.com/hc/en-us/articles/360015489531-Getting-Started-With-MetaMask>`_ and do that). 
| Copy your address wallet by simply clicking on your Account name...

.. image:: ../images/metamask.jpg
    :align: center
    :width: 400px

| ...and enter it into field on the faucet site. It can take from several minutes to few hours of waiting, depending on how long address queue is. 
|
| We've created two tokens in purpose of testing providing liquidity, resident pools working and so on. We've named them TSST and BeSt.
| Within yield.vote we use 3 kind of pools:

    1. Basic
    2. Resident
    3. Tycoon (not available yet)
   
| Basic pool requires FVT to play, while Resident requires LP tokens to do so. 


Swapping for FVT (or TSST and BeSt)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| On the main yield.vote page, click `Buy FVT Tokens` button.

.. image:: ../images/0_howto_main.jpg
    :align: center
    :width: 85%

| It will redirect you to `Uniswap app <https://app.uniswap.org/#/swap?use=V2&outputCurrency=0xF7eF90B602F1332d0c12cd8Da6cd25130c768929&inputCurrency=ETH>`_ where you can exchange ETH for FVT, TSST or BeSt.

.. important:: Make sure you are on Uniswap V2, otherwise you'll get "No liquidity" message.

    .. image:: ../images/1_howto_v2.png
        :align: center
        :width: 400px
    
| Don't spend all of your ether, because you'll need some to cover transaction fees. We recommend swapping 1 or less Ether for now.
|
| If there is no token in the bottom field or there is different than FVT, then simply click in the wrong or missing currency button and from the list choose FVT or one of the test tokens TSST or BeSt...

.. image:: ../images/1_howto_uniswap1.png
    :align: center
    :width: 85%

| ...but if it is not there you'll need to add it manually. 

.. |UNI1| image:: ../images/1_howto_uniswap3.jpg
    :width: 25%
    
.. |UNI2| image:: ../images/1_howto_uniswap2.jpg
    :width: 25%
    
.. |UNI3| image:: ../images/1_howto_uniswap4.jpg
    :width: 32%

.. admonition:: Tokens Addresses

    FVT token address::

        0xf7ef90b602f1332d0c12cd8da6cd25130c768929
        
    TSST token address::
        
        0xBFe5C40d16Be2EC652A627b1c2922B076DE267fD

    BeSt token address::
    
        0x46a0b72f44c0B380e85BE1215E3F7CAb1efaF046


| Click on **Manage token list**:

.. _Managetoken:

.. admonition:: Steps

    |UNI1| ---> |UNI2| ---> |UNI3|

    To use test tokens, do the same for TSST and/or BeSt.

    When it's done, you should be able to swap ETH for FVT, TSST or BeSt:

       1. Simply put interesting you amount of ETH in the ETH field (like 0.8 for example), the value of the exchanged cryptocurrency will be calculated automatically.

       2. Click on ``swap`` button.

       3. In the pop up window there are information on Liquidity Provider Fee, Price Impact, Minimum received and slippage tolerance. You can take a minute to analyse them, then hit ``confirm swap`` button.

       4. Now confirm transaction in MetaMask.

       5. After it's confirmed and no error occurs, in the upper right corner you should see ``Pending`` notice. Wait several seconds (sometimes it is a little longer than that), then check on your balance if ETH were substracted from it. If so, you should have exchanged cryptocurrency in your wallet.
    
.. |MM| image:: ../images/metamask_token.png
    :width: 30%


.. important:: You will probably want to add FVT, TSST and BeSt tokens to your MetaMask wallet, as you added to swap form. Simply hit ``Add Token`` button in Assets tab in MetaMask, then put token address in there. |MM|

Getting LP tokens
^^^^^^^^^^^^^^^^^^^^
| To receive LP tokens, user needs to provide liquidity to one of the pools. 
| We operate on uniswap and sushiswap pools.
|
| Before you enter the pool, you'll have to get some TSST and BeSt tokens. Below list of our test pools and information on token they are using:

1. Basic::
   
   * FVT Basic      -   FVT

2. Resident::
   
   * FVT Resident   -   LP token     (FVT/ETH)
   * TSST sushi     -   SLP token    (TSST/ETH sushiswap pool)
   * TSST uni       -   UNI-V2 token (TSST/ETH uniswap pool)
   * BeSt uni       -   UNI-V2 token (BeSt/ETH uniswap pool)
   * Resident new   -   UNI-V2 token (BeSt/ETH uniswap pool)

.. warning:: Even when they take the same pair of tokens, pools on sushiswap and uniswap ARE NOT THE SAME POOLS. LP tokens gained from them won't sum together. You'll have SLP gained from sushi pool and UNI-V2 gained from uni pool listed separately on your token list.

    Also You'll have 2 separated UNI-V2 positions in wallet, since their pools takes different pairs of tokens.

Uniswap pool
**************
1. To access uniswap pool you can click on the main yield.vote site ``provide liquidity for FVT`` button or `click here <https://app.uniswap.org/#/add/v2/ETH>`_ or in uniswap app choose pool -> view v2 liquidity -> add v2 liquidity.
2. From bottom token selector choose BeSt or TSST (depending which LP token you need, see list above).
3. Type in either ETH value or BeSt(or TSST) value, the second one will calculate itself automatically. If everything will be okay then you should be able to see “Approve BeSt” (or TSST) button.
   
    .. image:: ../images/2_howto_unipool.jpg
4. MetaMask pop up window will appear. Click confirm.
5. When BeSt/TSST will get approved (it could take up to several minutes) you can supply to the pool.
    
    .. image:: ../images/2_howto_unipool2.jpg
6. Confirmation window will pop up with additional information on LP tokens amount receive, rates, share of the pool and amount of tokens supplied. Click ``Confirm supply`` button.

    .. image:: ../images/2_howto_unipool3.jpg
        :width: 200px
        :align: center

7. Another confirmation window will pop up, confirm in MetaMask. You can click "Add Uni-V2 token to MetaMask", which we recommend to click. 
8. When transaction gets approved LP tokens will be added to your balance.

Sushiswap pool
***************
.. |SU1| image:: ../images/3_howto_sushipool.jpg
    :width: 49%

.. |SU2| image:: ../images/3_howto_sushipool2.jpg
    :width: 49%

| For now, we only use TSST/ETH pool on sushiswap. If you don't see TSST token on token selector list, check :ref:`manage token list in swapping tokens section<Managetoken>`. Method to add TSST token in sushiswap is identical to method in uniswap.

1. To access sushipool click `here <https://app.sushi.com/add/ETH/0xBFe5C40d16Be2EC652A627b1c2922B076DE267fD>`_ or in sushi app choose pool -> add.
2. From bottom token selector choose TSST.
3. Type in either ETH or TSST value, the second one will calculate itself automatically. If everything will be okay then you should be able to see “Approve TSST" button.
4. MetaMask pop up window will appear. Click confirm.
5. When TSST will get approved (it could take up to several minutes) you can supply to the pool, by clicking ``Confirm Adding Liquidity`` button. 

   .. important:: If ETH or TSST value will be too high, Adding Liquidity button won't appear. Make sure you are not trying to add funds above your limit. 

       |SU1| |SU2|

   
6. Confirmation window will pop up with additional information on LP tokens amount receive, rates, share of the pool and amount of tokens supplied. Click ``Confirm supply`` button.   
7. Waiting for confirmation window will be shown and MetaMask confirmation window will pop up. Click confirm. 
8. When transaction gets approved LP tokens will be added to your balance.
   
LP Tokens Addresses
^^^^^^^^^^^^^^^^^^^^
| In case you didn't add LP tokens to your wallet after liquidity providing confirmation or it didn't work, here we give LP tokens addresses:

.. admonition:: LP tokens addresses

    TSST/ETH sushi (SLP)::

        0x485345705b85a416fa1276fbca7fb81f3ebd9d13

    BeSt/ETH sushi (SLP)::

        0xc3795a945923914f0e5e4cf45c68f523fe835238

    TSST/ETH uni (UNI-V2)::

        0xf5a9c4e4fc81a9f628058c18322fd1d7f51452a0

    BeSt/ETH uni (UNI-V2)::

        0xa34cab81aa58cef453a73cdfe5600b9b699f9d66

